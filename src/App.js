import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AddCategories from './components/AddCategories';
import Contact from './components/Contact';
import Login from './components/Login';
import Profile from './components/Profile';
import Setting from './components/Setting';
import Nav from './Nav';

export class App extends Component {
  render() {
    return (
      <Router>
        <>
          <Nav />
          <Switch>
            <Route path="/" exact component={Login} />
            <Route path="/profile" component={Profile} />
            <Route path="/setting" component={Setting} />
            <Route path="/logout" component={this.logout} />
            <Route path="/categories" component={AddCategories} />
            <Route path="/contact" component={Contact} />
          </Switch>
        </>
      </Router>
    )
  }
}

export default App;