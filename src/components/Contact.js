import React, { useState } from 'react';
import axios from 'axios';
import '../App.css'

function Contact() {
  const [name, setName] = useState()
  const [email, setEmail] = useState()
  const [message, setMessage] = useState()
  const [number, setNumber] = useState()

  function onName(e) {
    setName(e.target.value)
  }

  function onEmail(e) {
    setEmail(e.target.value)
  }
  function onMessage(e) {
    setMessage(e.target.value)
  }
  function onNumber(e) {
    setNumber(e.target.value)
  }

  function clearForm() {
    setNumber('')
    setName('')
    setEmail('')
    setMessage('')
  }

  function onSubmit(e) {
    e.preventDefault();
    const data = {
      name, email, number, message
    }

    axios.post('http://localhost:5000/user/sendMail', data)
      .then((res) => {
        clearForm()
        alert(res.data)
      })
      .catch(err => console.log(err))
  }

  return (
    <div className="container">
      <div className="text">
        Contact Form</div>
      <form onSubmit={onSubmit}>
        <div className="data">
          <label>Name:</label>
          <input type="text" required value={name} onChange={onName} />
        </div>
        <div className="data">
          <label>Email:</label>
          <input type="email" required value={email} onChange={onEmail} />
        </div>
        <div className="data">
          <label>Mobile:</label>
          <input type="text" required value={number} onChange={onNumber} />
        </div>
        <div className="data">
          <label>Message:</label>
          <textarea value={message} required onChange={onMessage}></textarea>
        </div>
        <div className="btn">
          <div className="inner">
          </div>
          <button type="submit">Send</button>
        </div>
      </form>
    </div>
  )
}

export default Contact
