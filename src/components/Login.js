import React, { Component } from 'react';
import '../App.css';
import firebase from '../firebase'

export class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      value: '',
      isLoggedIn: false,
      name: '',
      photo: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.googleLog = this.googleLog.bind(this);
    this.logout = this.logout.bind(this)
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleClick = () => {
    var recaptcha = new firebase.auth.RecaptchaVerifier('recaptcha');
    var number = '+91' + this.state.value;
    console.log(number)
    firebase.auth().signInWithPhoneNumber(number, recaptcha).then(function (e) {
      var code = prompt('enter the otp', '');
      if (code === null) return;
      e.confirm(code).then(function (result) {
        console.log(result.user, 'user');
        document.querySelector('p').textContent += result.user.phoneNumber + " Number Verified"
      }).catch((error) => {
        console.log(error)
      })
    }).catch(function (err) {
      console.log(err)
    })
  }

  googleLog = () => {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function (result) {
      // This gives you a Google Access Token. You can use it to access the Google API.
      var token = result.credential.accessToken;
      // The signed-in user info.
      var user = result.user;
      // ...
    }).catch(function (error) {
      console.log(error)
    });
  }

  componentDidMount = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log("user signed in")
        console.log(user.displayName + '\n' + user.email)
        console.log(user)
        this.setState({
          isLoggedIn: true,
          name: user.displayName,
          photo: user.photoURL
        })
      } else {
        console.log("No User is signed in")
      }
    })
  }

  logout = () => {
    firebase.auth().signOut().then(function () {
      // Sign-out successful.
    }).catch(function (error) {
      // An error happened.
    });
    this.setState({
      isLoggedIn: false
    })
  }

  render() {
    return (
      <div>
        {this.state.isLoggedIn === false ?
          <div className="container">
            <div id="recaptcha"></div>
            <p></p>
            <div className="text">
              Login Form</div>
            <form>
              <div className="data">
                <label>Phone</label>
                <input type="text" value={this.state.value} onChange={this.handleChange} />
              </div>
              <div className="btn">
                <div className="inner">
                </div>
                <button type="button" onClick={this.handleClick}>login</button>
              </div>
            </form>
            <div className="btn">
              <div className="inner">
              </div>
              <button><a onClick={this.googleLog}>Google</a></button>
            </div>
            <div className="btn">
              <div className="inner">
              </div>
              <button><a >Facebook</a></button>
            </div>
          </div>
          : <div className="container">
            <button className="btn inner" type="button" onClick={this.logout}>Signout</button>
            <img src={this.state.photo} alt="" />
            <div className="text">
              {this.state.name}
            </div>
          </div>}
      </div>
    )
  }
}

export default Login
