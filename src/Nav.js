import React from 'react';
import './App.css';
import { Link } from 'react-router-dom'

function Nav() {
  return (
    <nav className="Nav">
      <Link to="/">
        <h1>Logo</h1>
      </Link>
      <ul>
        <Link to="/profile">
          <li>Profile</li>
        </Link>
        <Link to="/setting">
          <li>Setting</li>
        </Link>
        <Link to="/categories">
          <li>Add Categories</li>
        </Link>
        <Link to="/contact">
          <li>Contact</li>
        </Link>
        <Link to="/">
          <li>LogOut</li>
        </Link>
      </ul>
    </nav>
  )
}

export default Nav;